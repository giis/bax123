randomNumber=$(( $RANDOM % 30 + 40 )); #random number that starts at 40 and goes until 70
guess=1 #starting guess
for (( ; ; )) #infinity for loop
do
read -p "This is guess number $guess, Guess my number, it is between 40-70 : " userNumber #taking the guess from the user

if [ $userNumber -eq $randomNumber ]
then
echo "your guess is correct"
echo "WELL DONE, you guessed this correctly with only $guess guesses taken" #report of the 'game' is given before it breaks from the script
break
else
echo "your guess is wrong"
if [ $userNumber -ge $randomNumber ]
then
echo "try a lower number than $userNumber"
else
echo "try a higher number than $userNumber"

if [ $guess -eq 10 ]
then
echo "you have run out of guesses, then number was $randomNumber"
break
fi
fi
fi
guess=$((guess+1))
done
